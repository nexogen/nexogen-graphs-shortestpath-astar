/**
 *
 * NEXOGEN CONFIDENTIAL
 * ____________________
 *
 *  [2011] - [2017] Mark Farkas, Nexogen Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Mark Farkas and Nexogen Limited and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Mark Farkas and Nexogen Limited
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from either Mark Farkas or Nexogen Limited.
 */

'use strict'

const Heap = require('heap')

const calculateShortestPathAStar = (start, goal, distanceBetween, neighborsOf, heuristicCostEstimate) => {
  if (heuristicCostEstimate === undefined) {
    heuristicCostEstimate = (v1, v2) => 0.0
  }
  // the set of nodes already evaluated.
  const closedSet = new Set()
  // the set of currently discovered nodes that are not evaluated yet.
  // initially, only the start node is known.
  const openSet = new Set([start])
  // for each node, which node it can most efficiently be reached from.
  // if a node can be reached from many nodes, cameFrom will eventually
  // contain the most efficient previous step.
  const cameFrom = new Map()
  // gScore, fScore
  // for each node, the cost of getting from the start node to that node.
  // the cost of going from start to start is zero.
  const gScore = scoreMapFactory([[start, 0.0]])
  // for each node, the total cost of getting from the start node to the goal
  // by passing by that node. that value is partly known, partly heuristic.
  // for the first node, that value is completely heuristic.
  const fScore = scoreMapFactory([[start, heuristicCostEstimate(start, goal)]])
  const openHeap = new Heap((v1, v2) => fScore.get(v1) - fScore.get(v2))
  openHeap.push(start)
  // repeat until no more nodes are to be discovered
  while (openSet.size > 0) {
    const current = openHeap.pop()
    if (current === goal) {
      return reconstructShortestPath(cameFrom, current)
    }
    openSet.delete(current)
    closedSet.add(current)
    const currentGScore = gScore.get(current)
    for (let neighbor of neighborsOf(current)) {
      if (closedSet.has(neighbor)) {
        // ignore the neighbor which is already evaluated.
        continue
      }
      // the distance from start to a neighbor
      const tentativeGScore = currentGScore + distanceBetween(current, neighbor)
      let update = false
      if (!openSet.has(neighbor)) {
        // discover a new node
        openSet.add(neighbor)
      } else {
        if (tentativeGScore >= gScore.get(neighbor)) {
          // this is not a better path
          continue
        }
        update = true
      }
      // this path is the best until now. record it!
      cameFrom.set(neighbor, current)
      gScore.set(neighbor, tentativeGScore)
      fScore.set(neighbor, tentativeGScore + heuristicCostEstimate(neighbor, goal))
      if (update) {
        // re-build heap
        openHeap.updateItem(neighbor)
      } else {
        openHeap.push(neighbor)
      }
    }
  }
}

const scoreMapFactory = (iterable) => {
  const scoreMap = new Map(iterable)
  return {
    get: (node) => scoreMap.has(node) ? scoreMap.get(node) : Infinity,
    set: function (node, value) {
      scoreMap.set(node, value)
    }
  }
}

const reconstructShortestPath = (cameFrom, current) => {
  const totalPath = [current]
  while (cameFrom.has(current)) {
    current = cameFrom.get(current)
    totalPath.unshift(current)
  }
  return totalPath
}

module.exports = exports = {
  AStar: {
    calculateShortestPath: calculateShortestPathAStar
  }
}
