/**
 *
 * NEXOGEN CONFIDENTIAL
 * ____________________
 *
 *  [2011] - [2017] Mark Farkas, Nexogen Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Mark Farkas and Nexogen Limited and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Mark Farkas and Nexogen Limited
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from either Mark Farkas or Nexogen Limited.
 */

'use strict'

