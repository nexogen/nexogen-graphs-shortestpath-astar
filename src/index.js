/**
 *
 * NEXOGEN CONFIDENTIAL
 * ____________________
 *
 *  [2011] - [2017] Mark Farkas, Nexogen Ltd.
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Mark Farkas and Nexogen Limited and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Mark Farkas and Nexogen Limited
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from either Mark Farkas or Nexogen Limited.
 */

const shortestPathAlgorithms = require('./shortestPathAlgorithms')

const start = 0
const goal = 15

const distanceBetween = (v1, v2) => Math.abs(v1 - v2) === 1 ? 1 : (goal - start) + 1

const heuristicCostEstimate = (v1, v2) => 0.0

const neighborsOfNodeInFullConnectedGraph = (first, last, next, node) =>
  (function* () {
    let neighbor = first
    while (neighbor !== next(last)) {
      if (neighbor !== node) {
        yield neighbor
      }
      neighbor = next(neighbor)
    }
  })()

const neighborsOf = (node) => neighborsOfNodeInFullConnectedGraph(start, goal, current => current + 1, node)

console.time('shortestPathAStar')
var shortestPathFromStartToGoal = shortestPathAlgorithms.AStar.calculateShortestPath(start, goal, distanceBetween, neighborsOf, heuristicCostEstimate)
console.timeEnd('shortestPathAStar')
console.log(shortestPathFromStartToGoal)

